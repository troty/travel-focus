"use strict";

function processMobileNavigation() {
	$(".navbar").each(function() {
		var children = $("li.nav-item.dropdown > a");
		var CloneButton = $('<button type="button" class="toggleSubmenu"><i class="fas fa-chevron-down"></i></button>')
		$(children).after($(CloneButton));
        //show/hide submenu nav
		$(	".toggleSubmenu" ).click(function() {
			$(this).parent().toggleClass( "show" );
			$(this).next().toggleClass( "show" );
		});
	});
}
function homepageSlider() {	
	$('.slider').slick({
		infinite: true,
		dots: true,
		arrows: true,
		autoplay: true,
		autoplaySpeed: 4000,
		speed: 500,
  		fade: true		
	});
}
function quotesSlider() {	
	$('.quotes_slider').slick({
		infinite: true,
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 4000,
		speed: 500,
		centerMode: true,
		centerPadding: '25%',
		slidesToShow: 1,
		initialSlide: 1,
		responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	      	centerPadding: '20%',
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	      	centerPadding: '15%',
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        centerPadding: '10%',
	      }
	    }
	  ]
	});
}
function tipsSlider() {	
	$('.slider_tips').slick({
		infinite: true,
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 4000,
		speed: 500,
		slidesToShow: 3,
		responsive: [	    
	    {
	      breakpoint: 767,
	      settings: {
	      	slidesToShow: 2,
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	      }
	    }
	  ]
	});
}
function customSlider() {	
	$('.slider_custom').slick({
		infinite: true,
		dots: true,
		arrows: true,
		autoplay: true,
		autoplaySpeed: 4000,
		speed: 600,
		centerMode: true,
		centerPadding: '22%',
		slidesToShow: 1,
		initialSlide: 1,
		cssEase: 'cubic-bezier(0.950, 0.050, 0.795, 0.035)',
		responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	      	centerPadding: '20%',
	      }
	    },
	    {
	      breakpoint: 576,
	      settings: {
	      	centerPadding: '15%',
	      }
	    }
	  ]
	});
}
function SliderBig() {	
	$('.slider_big').slick({
		infinite: true,
		dots: true,
		arrows: true,
		autoplay: false,
		autoplaySpeed: 4000,
		speed: 600,
		slidesToShow: 1,
	});
}
function callbackToTop() {
	var scrollTrigger = 800,
	backToTop = function () {
		var scrollTop = $(window).scrollTop();
		if (scrollTop > scrollTrigger) {
			$('#back-to-top').addClass('show');
		} else {
			$('#back-to-top').removeClass('show');
		}
	};
	backToTop();
	$(window).on('scroll', function () {
		backToTop();
	});
	$('#back-to-top').on('click', function (e) {
		e.preventDefault();
		$('html,body').animate({
			scrollTop: 0
		}, 700);
	});
}
function scrollTo() {
	$("a.scroll_to").click(function(e) {
		e.preventDefault();
		var section = $(this).attr("href");
		$("html, body").animate({
			scrollTop: $(section).offset().top - 110
		});
		return;
	});
}
$(function() {
	ScrollReveal().reveal('.slider__container', { origin: 'bottom', distance: '20px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)'},120);
	ScrollReveal().reveal('.module_01, .module_02, .module_03, .module_06 article, .module_footer', { origin: 'bottom'}, 50);
	ScrollReveal().reveal('.module_01 .content_wrap', { origin: 'bottom', distance: '20px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)'},120);
	ScrollReveal().reveal('.module_02 .content_wrap , .split_items .content_wrap', { origin: 'bottom', interval: 15, distance: '120px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)'},180);
	ScrollReveal().reveal('.module_03 .intro', 50);
	ScrollReveal().reveal('.module_03 .item', { origin: 'bottom', interval: 16, distance: '60px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',},100);
	ScrollReveal().reveal('.module_03 .subtitle', { origin: 'bottom', interval: 15, distance: '20px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)', },50);
	ScrollReveal().reveal('.module_03 .heading_two', { origin: 'bottom', distance: '20px', delay: '120',});
	ScrollReveal().reveal('.module_03 .trendline', { origin: 'bottom', distance: '20px', delay: '150', });
	ScrollReveal().reveal('.module_03 .intro .punchline', { origin: 'bottom', distance: '20px', delay: '150', });
	ScrollReveal().reveal('.module_footer .pilon', { origin: 'bottom', interval: 26, delay: '250', distance: '60px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',},100);
    ScrollReveal().reveal('.animated-image', { origin: 'bottom', interval: 26, distance: '200px', delay: '250',});
    ScrollReveal().reveal('.animated-content', { origin: 'left', distance: '200px', easing: 'cubic-bezier(0.6, 0.2, 0.8, 1)', delay: '100', });
    ScrollReveal().reveal('.module_09 .box', { origin: 'bottom', interval: 26, delay: '250', distance: '60px', easing: 'cubic-bezier(0.6, 0.2, 0.1, 1)',},100);

    // ::: Hamburger Toggle
    $('[data-toggle="menu-offcanvas"]').on("click", function() {
        $(this).toggleClass("is-active");
        $(this)
            .next("#offcanvas-navbar")
            .toggleClass("open");
    });
    
    homepageSlider();
    processMobileNavigation();
	renderInlineSVG();
	callbackToTop();
	scrollTo();
	quotesSlider();
	tipsSlider();
	customSlider();
	SliderBig();
});


// ::: Replace all SVG images with inline SVG
function renderInlineSVG() {
	jQuery("img")
		.filter(function() {
			return this.src.match(/.*\.svg$/);
		})
		.each(function() {
			var $img = $(this);
			var imgID = $img.attr("id");
			var imgClass = $img.attr("class");
			var imgURL = $img.attr("src");

			$.get(
				imgURL,
				function(data) {
					// Get the SVG tag, ignore the rest
					try {
						var $svg = jQuery(data).find("svg");

						// Add replaced image's ID to the new SVG
						if (typeof imgID !== "undefined") {
							$svg = $svg.attr("id", imgID);
						}
						// Add replaced image's classes to the new SVG
						if (typeof imgClass !== "undefined") {
							$svg = $svg.attr("class", imgClass + " replaced-svg");
						}

						// Remove any invalid XML tags as per http://validator.w3.org
						$svg = $svg.removeAttr("xmlns:a");

						// Check if the viewport is set, if the viewport is not set the SVG wont't scale.
						if (
							!$svg.attr("viewBox") &&
							$svg.attr("height") &&
							$svg.attr("width")
						) {
							$svg.attr(
								"viewBox",
								"0 0 " + $svg.attr("height") + " " + $svg.attr("width")
							);
						}

						// Replace image with new SVG
						$img.replaceWith($svg);
					} catch (ex) {
						/* Ignore any errors and leave the img as is */
					}
				},
				"xml"
			);
		});
}
// ::: GD - Close Cookie Policy
function closeCookiePolicy(){
	jQuery.ajax({
		type: "GET",
		url: "index.cfm?action=ajax_removeCookiePolicy",
		success: function (response) {
			$("#stickycookiePolicy").fadeOut();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(thrownError);
			console.log(xhr.responseText);
		}
	});
}